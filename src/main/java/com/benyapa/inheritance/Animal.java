/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.benyapa.inheritance;

/**
 *
 * @author bwstx
 */
public class Animal {

    protected String name;
    protected int numOfLegs = 0;
    protected String color;

    public Animal(String name, String color, int numOfLegs) {
        System.out.println("Animal created");
        this.name = name;
        this.color = color;
        this.numOfLegs = numOfLegs;
    }

    public void walk() {
        System.out.println("Animal walk");
    }

    public void speak() {
        System.out.println("Animal speak");
        System.out.println("name: " + this.name + " color : " + this.color + " numOfLegs : " + this.numOfLegs);
    }

    public void printLine() {
        System.out.println("----------");
    }

    public String getName() {
        return name;
    }

    public int getNumOfLegs() {
        return numOfLegs;
    }

    public String getColor() {
        return color;
    }

}
