/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.benyapa.inheritance;

/**
 *
 * @author bwstx
 */
public class TestAnimal {

    public static void main(String[] args) {
        Animal animal = new Animal("Ani", "White", 0);
        animal.speak();
        animal.walk();
        animal.printLine();

        Dog dang = new Dog("Dang", "Black & White");
        dang.speak();
        dang.walk();
        dang.printLine();

        Dog to = new Dog("To", "Orange");
        to.speak();
        to.walk();
        to.printLine();

        Dog mome = new Dog("Mome", "Black & White");
        mome.speak();
        mome.walk();
        mome.printLine();

        Dog bat = new Dog("Bat", "Black & White");
        bat.speak();
        bat.walk();
        bat.printLine();

        Cat zero = new Cat("Zero", "Orange");
        zero.speak();
        zero.walk();
        zero.printLine();

        Duck som = new Duck("Som", "Orange");
        som.speak();
        som.walk();
        som.fly();
        som.printLine();
        
        Duck gabgab = new Duck("GabGab", "Black");
        gabgab.speak();
        gabgab.walk();
        gabgab.fly();
        gabgab.printLine();
        
//checkDuck
        System.out.println("Som is Animal :" + (som instanceof Animal));
        System.out.println("Som is Duck :" + (som instanceof Duck));
        System.out.println("Som is Cat :" + (som instanceof Object));
        System.out.println("Som is Dog :" + (som instanceof Object));
        System.out.println();
        System.out.println("GabGab is Animal :" + (gabgab instanceof Animal));
        System.out.println("GabGab is Duck :" + (gabgab instanceof Duck));
        System.out.println("GabGab is Cat :" + (gabgab instanceof Object));
        System.out.println("GabGab is Dog :" + (gabgab instanceof Object));
        System.out.println();
//checkDog
        System.out.println("To is Animal :" + (to instanceof Animal));
        System.out.println("To is Duck :" + (to instanceof Object));
        System.out.println("To is Cat :" + (to instanceof Object));
        System.out.println("To is Dog :"+(to instanceof Dog));
        System.out.println();
        System.out.println("Mome is Animal :" + (mome instanceof Animal));
        System.out.println("Mome is Duck :" + (mome instanceof Object));
        System.out.println("Mome is Cat :" + (mome instanceof Object));
        System.out.println("Mome is Dog :"+(mome instanceof Dog));
        System.out.println();
        System.out.println("Bat is Animal :" + (bat instanceof Animal));
        System.out.println("Bat is Duck :" + (bat instanceof Object));
        System.out.println("Bat is Cat :" + (bat instanceof Object));
        System.out.println("Bat is Dog :"+(bat instanceof Dog));
        System.out.println();
        System.out.println("Dang is Animal :" + (dang instanceof Animal));
        System.out.println("Dang is Duck :" + (dang instanceof Object));
        System.out.println("Dang is Cat :" + (dang instanceof Object));
        System.out.println("Dang is Dog :"+(dang instanceof Dog));
        System.out.println();
//checkCat
         System.out.println("Zero is Animal :" + (zero instanceof Animal));
        System.out.println("Zero is Duck :" + (zero instanceof Object));
        System.out.println("Zero is Cat :" + (zero instanceof Cat));
        System.out.println("Zero is Dog :"+(zero instanceof Object));
        System.out.println();
        
        System.out.println("Animal is Dog :" + (animal instanceof Dog));
        System.out.println("Animal is Cat :" + (animal instanceof Cat));
        System.out.println("Animal is Duck :" + (animal instanceof Duck));
        System.out.println("Animal is Animal :" + (animal instanceof Animal));
        

        System.out.println();
        
        Animal[] animals = {dang, zero, som,mome,bat,to,gabgab};
        for (int i = 0; i < animals.length; i++) {

            if (animals[i] instanceof Duck) {
                Duck duck = (Duck) animals[i];
                duck.fly();
            }
            System.out.println();
            if (animals[i] instanceof Dog) {
                Dog dog = (Dog) animals[i];
                 animals[i].walk();
                 animals[i].speak();
            }
            System.out.println();
            if (animals[i] instanceof Cat) {
                Cat cat = (Cat) animals[i];
                 animals[i].walk();
                 animals[i].speak();
            }
        }
    }

}
